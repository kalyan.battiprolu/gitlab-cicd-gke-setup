# gitlab-cicd-gke-setup

This project helps you to set up GitLab integration with existing Google Kubernetes Engine(GKE) along with gitlab account

Pre-requisites

1. Gitlab Account
2. Google Kubernetes Engine
3. Gitlab Agent setup on GKE Cluster
4. Setup Gitlab Runner on GKE
5. Create Service account and role for Gitlab runner


## Getting started

Make sure your gitlab account is CICD enabled check below URL for guidance,and we are using GitLab CI/CD workflow steps to enable CICD with GKE there are two flows available in Gitlab as below

https://docs.gitlab.com/ee/ci/enable_or_disable_ci.html#enable-cicd-in-a-project


1. GitOps Works Flow (Limited to Premium tier) - link https://docs.gitlab.com/ee/user/clusters/agent/gitops.html
2. GitLab CI/CD workflow - All tiers (even free gitlab tier also can use this) - https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html


## GitLab CI/CD workflow

Install gitlab agent in existing google kubernetes cluster using below link

https://docs.gitlab.com/ee/user/clusters/agent/install/index.html

1. create below directory in your source code root folder as below and speicfy any agent-name, in my case i am giving gke-cicd

.gitlab/agents/<agent-name>

refer the below link how i have created git lab agentfile

https://gitlab.com/kalyan.battiprolu/redis/-/tree/main/.gitlab/agents/gke-cicd

2. create config.yaml under this directory , use below reference example file for config.yaml content and replace the path with your project directory path

https://gitlab.com/gitlab-org/configure/examples/kubernetes-agent/-/blob/master/.gitlab/agents/agent1/config.yaml

3. Register an Agent as specified in the agent installation link above

## Considering gitlab free tier we are using manual gitlab runners instead of shared(to avoid registering credit info to authorize the gitlab account)

setup manual git runner as specified in the below link and customize your values.yaml file as per your requirment for the gitlab runner setup

make sure you created namespace for gitlab runner(optional)

https://docs.gitlab.com/runner/install/kubernetes.html

use below example command to setup the gitlab-runner

helm install -f ./values.yaml kube-runner gitlab/gitlab-runner -n gitlab-runner

once gitlab-runner is deployed on kubenetes cluster verify your gitlab runners for successfull registratoin of runner, use below link to verify the runners in gitlab

https://docs.gitlab.com/ee/ci/quick_start/#ensure-you-have-runners-available

## create required service account on kubernetes enginer for gitlab runner 

use below sample yaml files to create role and rolebinding files from this gitrepo and make appropriate changes to name and namespace and role names 

role-creation.yaml
rolebinding.yaml

## Deployment

create gitlab-ci.yml file in gitlab project root folder, add your build,test,deployment steps for the deployment
if your using helm templates create helm templates and kept under the same git source root directory

reference link to create gitlab-ci.yaml file

https://docs.gitlab.com/ee/ci/yaml/index.html









